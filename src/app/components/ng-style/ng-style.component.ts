import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.css']
})
export class NgStyleComponent implements OnInit {

  tamano: number = 20;
  constructor() { }

  ngOnInit() {
  }

  aumentarTamano() {
    this.tamano += 5;
  }
  disminuirTamano() {
    this.tamano = this.tamano - 5;
  }

}
