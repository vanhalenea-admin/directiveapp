import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.css']
})
export class NgClassComponent implements OnInit {

  public clase: string;
  public conditionNgClass: boolean = false;
  public loading: boolean  false;
  constructor() { }

  ngOnInit() {
    this.clase= 'alert-danger';
  }

  ejecutarAsync(){
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
    }, 3000);

  }

}
