import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  @Input('appResaltado') color;
  constructor(private el: ElementRef) { 
    console.log('directiva llamada');
    
  }

@HostListener('mouseenter') mouseEnter() {
  console.log(this.color);
  this.el.nativeElement.style.backgroundColor = this.color;
}
@HostListener('mouseleave') mouseLeave() {
  this.el.nativeElement.style.backgroundColor = "white";
}

}
