import { UsuarioDetalleComponent } from './components/usuario-detalle/usuario-detalle.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { UsuarioNuevoComponent } from './components/usuario-nuevo/usuario-nuevo.component';
import { UsuarioEditarComponent } from './components/usuario-editar/usuario-editar.component';



export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { 
    path: 'usuario/:id',  
    component: UsuarioComponent ,
    children : [
      { path: 'nuevo',  component: UsuarioNuevoComponent },
      { path: 'editar',  component: UsuarioEditarComponent },
      { path: 'detalle',  component: UsuarioDetalleComponent },
    ]
  },
  { path: '**',    component: HomeComponent },
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES);